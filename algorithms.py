"""debug algorithms"""
## Sorting Algorithms - by Blake Romero ##

def bubble_sort(array,ascending=True):
    """Bubble sort algorithm - True for ascending (default), False for descending"""

    length = len(array) - 1                         # gets length of array (-1 for array index iteration)
    swap = False                                    # initialise swap boolean
    
    while length > 0:                               # while not on last value
        if ascending:
            for i in range(length):                     # iterate through array length
                if array[i] > array[i+1]:               # if adjacent is smaller, swap
                    array[i], array[i+1] = array[i+1], array[i]
                    if not swap:                        # if not swapped before; flag boolean
                        swap = True
        elif not ascending:
            for j in range(length):                     # iterate through array length
                if array[j] < array[j+1]:               # if adjacent is smaller, swap
                    array[j], array[j+1] = array[j+1], array[j]
                    if not swap:                        # if not swapped before; flag boolean
                        swap = True
        if not swap:                                # If no swaps at all, exit loop
            break
        else:
            length -= 1                             # separate sorted by decrementing length of array

def insertion_sort(array):
    """Insertion sort algorithm"""
    length = len(array)                             # gets length of array (-1 for array index iteration)
    for i in range(1, length):                      # from index 1 to length of array
        currentvalue = array[i]                     # record current value
        position = i                                # record position
        while position > 0:                         # while not last position (most left)
            if array[position-1] > currentvalue:    # if left value > right value, swap from current position
                array[position], array[position-1] = array[position-1], array[position]
                position -= 1                       # decrement position counter
            else:                                   # if currentvalue biggest value, skip to next index
                break

def selection_sort(array):
    """Selection sort algorithm"""
    length = len(array)                             # gets length of array (-1 for array index iteration)
    for i in range(length-1):                       # iterate through array
        imin = i                                    # assign inital index as minimum value
        for j in range(i+1, length):                # iterate through adjacent values
            if array[j] < array[imin]:              # if adjacent smaller value
                imin = j                            # set target index as the smallest value
        if imin != i:                               # if smaller value in other index, swap values
            array[i], array[imin] = array[imin], array[i]

def merge_sort(alist):
    """Merge sort  algorithm"""
    if len(alist) > 1:
        mid = len(alist)//2
        lefthalf = alist[:mid]
        righthalf = alist[mid:]

        merge_sort(lefthalf)
        merge_sort(righthalf)

        i=0
        j=0
        k=0

        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                alist[k] = lefthalf[i]
                i += 1
            else:
                alist[k] = righthalf[j]
                j += 1
            k += 1

        while i < len(lefthalf):
            alist[k] = lefthalf[i]
            i += 1
            k += 1

        while j < len(righthalf):
            alist[k] = righthalf[j]
            j += 1
            k += 1
