################################################
## Debug Sorting Algorithms - by Blake Romero ##
################################################
#pylint: disable=all

def bubble_sort(array,ascending=True):
    """Bubble sort algorithm"""
    length = len(array) - 1                 # length of array (-1 for array index iteration)
    swap = False                            # initialise swap boolean
    comparisons = 0
    swaps = 0
    i=1
    while length > 0:                       # while not on last value
        if ascending:
            print("\nIteration",i)
            for j in range(length):             # iterate through array length
                comparisons += 1
                print("comparing:",array[j],array[j+1])
                if array[j] > array[j+1]:       # if adjacent is smaller, swap
                    array[j], array[j+1] = array[j+1], array[j]
                    print("SWAPPING:",array[j],array[j+1])
                    swaps += 1
                    if not swap:                # if not swapped before; flag boolean
                        swap = True
            print(array)
        if not ascending:
            print("\niteration",i)
            for j in range(length):             # iterate through array length
                comparisons += 1
                print("comparing:",array[j],array[j+1])
                if array[j] < array[j+1]:       # if adjacent is smaller, swap
                    array[j], array[j+1] = array[j+1], array[j]
                    print("SWAPPING:",array[j],array[j+1])
                    swaps += 1
                    if not swap:                # if not swapped before; flag boolean
                        swap = True
            print(array)
        if not swap:                        # If no swaps at all, exit loop
            print("Already sorted!")
            break
        else:
            length -= 1                     # separate sorted by decrimenting length of array
            i+=1
    return comparisons,swaps

def insertion_sort(array):
    """Insertion sort algorithm"""
    length = len(array)
    comparisons = 0
    swaps = 0
    for i in range(1, length):
        print(array)
        currentvalue = array[i]
        position = i
        while position > 0:
            if array[position-1] > currentvalue:
                comparisons += 1
                print(array[position-1], "IS >", currentvalue)
                array[position], array[position-1] = array[position-1], array[position]
                print("SWAPPED",array)
                swaps += 1
                position -= 1
            else:
                print(array[position-1], "is NOT >", currentvalue)
                comparisons += 1
                break
        print("Iteration", i, "complete")
    return comparisons,swaps

def selection_sort(array):
    """Selection sort algorithm"""
    length = len(array)                     # gets length of array (-1 for array index iteration)
    comparisons = 0
    print("Starting selection sort process")
    for i in range(length-1):               # iterate through array
        print(array)
        print("Iteration",i+1)
        imin = i                            # assign inital index as minimum value
        for j in range(i+1, length):        # iterate through adjacent values
            comparisons += 1
            print("comparing:", array[imin], array[j])
            if array[j] < array[imin]:      # if adjacent smaller value
                imin = j                    # set target index as the smallest value
                print(array[imin],"is the smallest value")
        if imin != i:                       # if smaller value in other index, swap values
            array[i], array[imin] = array[imin], array[i]
            print("SWAPPED",array[imin],array[i])
        print("Iteration", i+1, "complete")
    return comparisons

def merge_sort(alist):
    """Merge sort  algorithm"""
    print("Splitting ",alist)
    if len(alist) > 1:
        mid = len(alist)//2
        lefthalf = alist[:mid]
        righthalf = alist[mid:]

        merge_sort(lefthalf)
        merge_sort(righthalf)

        i=0
        j=0
        k=0

        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                alist[k] = lefthalf[i]
                i += 1
            else:
                alist[k] = righthalf[j]
                j += 1
            k += 1

        while i < len(lefthalf):
            alist[k] = lefthalf[i]
            i += 1
            k += 1

        while j < len(righthalf):
            alist[k] = righthalf[j]
            j += 1
            k += 1
            
        print("Merging ",alist)
