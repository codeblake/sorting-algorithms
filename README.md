# README #

A collection of sorting algorithms.

### Algorithms Covered ###

* Bubble sort
* Insertion sort
* Selection sort
* Merge sort