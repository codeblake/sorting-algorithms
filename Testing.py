import random
import debug_algorithms
#pylint: disable=C0326

array = [random.randint(1,100) for i in range(10)]
# la = [random.randint(1,100) for i in range(100)]
sortedArray = [1,2,3,4,5,6,7,8,9]

comparisons,swaps = debug_algorithms.bubble_sort(array)
print("\nTotal comparisons: " + str(comparisons))
print("Total swaps: " + str(swaps))
